# Season 8, Episode 03. Episode ID 128
--

What did we do in FOSS This Week?

---

## Topic

Podcast and Content Creation on Linux (via Fraggle on Discord)


Remind people that Tyler's audio crapped out last week because of NixOS.
  -- Nate
 
Ask for Apple Podcast reviews

10% off entire order on the shop with promo code WINTER24.

---
	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Email - email@thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---

# Thingies of the Week

Matt - Fable (book club app)

Tyler - Neovide
