
# Season 6, Episode 9. Episode ID 73
Topic from Matt

### What have we been up to Linux related this Week? ###

Tyler - I have updated my dotfiles, made dwm look sexy, and have been messing with my computer.

Matt – I learned a lot about polybar, padding and also cleaned up my i3 config. It is now less than 15 lines long. I've also been messing around with taskwarrior.
---

**Contact Info**

Twitter: @thelinuxcast

Subscribe at http://thelinuxcast.org

Contact us email@thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Tyler - https://bit.ly/3wk9LNy on Odysee, https://youtube.com/ZaneyOG on YouTube

Discord https://discord.gg/89FRrrcGhC

Telegram https://t.me/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://9to5linux.com/steam-deck-linux-powered-gaming-handheld-launches-officially
Tyler - https://9to5linux.com/mesa-22-0-officially-released-brings-vulkan-1-3-and-improves-support-for-many-games

---

### Main Topic ### 
Should You Use a Firewall?


---

### Apps of the Week ###

Matt - Link Shortener for Firefox, Chrome and Edge 

Tyler - Jitsi
