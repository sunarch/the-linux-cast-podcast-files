[#](#) Season 6, Episode 07. Episode ID 71
Topic from Tyler.
test



### What have we been up to Linux related this Week? ###

Tyler - I tried LFS, but I'm sticking to Gentoo. Really nice distro that I'm excited to see Matt use!

Matt – Ended my time with Manjaro. EndeavourOS is next. Moved from VirtualBox to Virt Manager.
---

**Contact Info**

Twitter: @thelinuxcast

Subscribe at http://thelinuxcast.org

Contact us email@thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Tyler - https://bit.ly/3wk9LNy on Odysee, https://bit.ly/3dbqbjX on YouTube

Discord https://discord.gg/89FRrrcGhC

Telegram https://t.me/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://news.itsfoss.com/mozilla-meta-facebook/ 
Tyler - https://9to5linux.com/valve-releases-proton-7-0-with-major-improvements-for-linux-gaming

---

### Main Topic ### 
Will Source Based Distros Ever Become Popular?


---

### Apps of the Week ###

Matt - Spyro Trilogy 

Tyler - Ranger
