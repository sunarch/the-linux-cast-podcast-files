# Season 6, Episode 30. Episode ID 94

### What have we been up to Linux related this Week? ###

Tyler - Fedora is my new family distro and played DayZ for around 8 hours last night.

Matt – Messing around with btrfs.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### Main Topic ### 
Are AppImages Good? 

#### Josh the Back Seat Host Checking In:

> App Images are... Alright. for the old school business model of downloading a paid for binary off the internet their basically the best option other than a deb or rpm file.
However this does not mean that their the best option for the end user. The biggets advantage the AppImage format has compared to snaps and even flatpaks is that AppImages support a manual upgrade process. How is this an advantage? Well that means you can find a version of kdenlive that works for you, and just stay on that one version of kdenlive.
The tradeoff is of course that same upgrade process of course. However the AppImage API does account for this, as evidenced by Bitwarden which will prompt to update. Plus the [third party tooling](https://github.com/AppImageCommunity/awesome-appimage) around AppImage is far and away better handled than flatpak and snap.
Personally, I do use AppImages. However I do wish the spec handled dropping a desktop file in /home/$user/.local/share/applications and not have to use a tool like AppImageLauncher (which has a systemd requirement) or a script to do it.
That said, I do actually have an appimage that contains st and firefox because sometimes thigns do break. And I even have a proprietary binary I package myself as an appimage so I dont have to use a Ubuntu docker image on my desktop.
That said, AppImages handle system theming, accessibility features, much better than flatpak and snaps. But that's because AppImage didn't take a "Secure By Default" approach it's competitors have.
Also Matt for the record, this is not a Gentoo Install stream. However I think thats fine as I have slowly and steadily been entering... The Void.

---

### Apps of the Week ###

Matt - Ferdium 

Tyler - Webcord
