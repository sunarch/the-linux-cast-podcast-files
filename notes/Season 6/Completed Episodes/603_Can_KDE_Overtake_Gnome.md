
# Season 6, Episode 03. Episode ID 67
Topic from Matt

### What have we been up to Linux related this Week? ###

Tyler - Made an astonashing amount of progress on my game and still not sure that's it's ready to show off.

Matt – Finishing up my MX Linux review and working on the website. 
---

**Contact Info**

Twitter: @thelinuxcast

Subscribe at http://thelinuxcast.org

Contact us email@thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Tyler - https://bit.ly/3wk9LNy on Odysee, https://bit.ly/3dbqbjX on YouTube

Discord https://discord.gg/89FRrrcGhC

Telegram https://t.me/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Also Check out the Merch store

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.omgubuntu.co.uk/2022/01/wine-7-0-released-with-tons-of-improvements-including-a-new-theme 
Tyler - https://www.gamingonlinux.com/2022/01/amd-ryzen-deskmini-um700-announced-with-manjaro-linux/

---

### Main Topic ### 
Can KDE Displace Gnome once again?


---

### Apps of the Week ###

Matt - qprompt 

Tyler - TreeIT
