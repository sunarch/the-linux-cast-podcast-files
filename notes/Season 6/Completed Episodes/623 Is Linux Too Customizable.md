
# Season 6, Episode 23. Episode ID 87

### What have we been up to Linux related this Week? ###

Tyler - Been relaxing on the beach with family and coming up with some video ideas.

Matt – I have had a hell of a week on Linux. Horrible. I'm now a Fedora user.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.omgubuntu.co.uk/2022/07/linux-mint-21-systemd-oom 
Tyler - https://www.tomshardware.com/news/intel-meteor-lake-igpu-linux

---

### Main Topic ### 
Is Linux Too Customizable?


---

### Apps of the Week ###

Matt - ghostwriter 

Tyler - dark reader
