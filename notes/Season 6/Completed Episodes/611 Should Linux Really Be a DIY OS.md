
# Season 6, Episode 11. Episode ID 75
Topic from Tyler

### What have we been up to Linux related this Week? ###

Tyler - I had issues on Gentoo, been gaming on Windows before coming back to OpenBSD.

Matt – I installed Gentoo. Also have been having issues. 
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://9to5linux.com/canonical-patches-dirty-pipe-vulnerability-in-ubuntu-21-10-and-20-04-lts-update-now 
Tyler - https://www.phoronix.com/scan.php?page=article&item=apple-m1-linux-perf&num=1

---

### Main Topic ### 
Is Linux From Scratch Really Feasible?


---

### Apps of the Week ###

Matt - ArcoLinux Tweak Tool and Desktop Trasher 

Tyler - pokemon-colorscripts
