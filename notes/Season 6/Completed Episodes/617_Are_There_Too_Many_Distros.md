
# Season 6, Episode 17. Episode ID 81

### What have we been up to Linux related this Week? ###

Tyler - Took a couple days off uploading and been test driving Fedora 36 Beta - it's incredible tbh.

Matt – Stuggling with samba. Riced i3 again. I also spent another week with Emacs. 
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.phoronix.com/scan.php?page=news_item&px=Linux-Encrypted-Hibernation 
Tyler - https://www.phoronix.com/scan.php?page=news_item&px=Rusticl-Darktable-Milestone

---

### Main Topic ### 



---

### Apps of the Week ###

Matt - Sonokai 

Tyler - Godot
