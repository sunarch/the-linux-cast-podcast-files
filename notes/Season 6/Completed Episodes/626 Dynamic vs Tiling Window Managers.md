
# Season 6, Episode 26. Episode ID 90

### What have we been up to Linux related this Week? ###

Tyler - I have been developing a game with the SteamDeck as it's main target platform. Also the SteamDeck is truly incredible.

Josh - I bought a [Pinebook Pro](https://www.pine64.org/pinebook-pro/) and [Pinephone Pro](https://www.pine64.org/pinephonepro/) and will be using them as a daily drivers for the next 30 days. No the earbuds don't run Gentoo.

Matt – I worked more on my ricing script. Did some research on NASs, and ended up buying a gigantic hard drive.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact

Find Josh - https://10leej.com/stalker

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.phoronix.com/news/NVIDIA-3D-Headers-Fermi-Ampere 

Tyler - https://www.bleepingcomputer.com/news/security/kali-linux-20223-adds-5-new-tools-updates-linux-kernel-and-more/

Josh - OpenSUSE has released a Release Candidate for [MicroOS Desktop](https://en.opensuse.org/Portal:MicroOS/Desktop) and it's the SUSE we all wanted it to be. No YaST to be found. Of course I'm working on porting it to the pinebook pro.

---

### Main Topic ### 
Dynamic vs Tiling Window Managers

Josh - Really there is no real efficiency benefit between manual and dynamic window managers. It purely a matter of workflow. However I'm strong in the Dynamic camp.

---

### Thingies of the Week ###

Matt - SyncThing GTK

Josh - [McAfee for Linux](https://www.mcafee.com/enterprise/en-us/downloads/trials/virusscan-enterprise-for-linux.html) actually exists, and of course I have a legit subscription. I blame the FDIC who legit requires a paid antivirus from an approved vendor even though I have SELinux. Don't ban me Matt please!

Tyler - SteamDeck Plugins
