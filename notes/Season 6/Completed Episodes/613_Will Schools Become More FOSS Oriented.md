
# Season 6, Episode 13. Episode ID 77
Topic from Tyler 




### What have we been up to Linux related this Week? ###

Tyler - Working on my mini itx build, and getting some Markdown guides ready for OpenBSD.

Matt – Moved away from vimwiki and back go Zim. I created a standing desk script. Also created a new end screen for the videos
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://linuxiac.com/fedora-plans-to-drop-support-for-legacy-bios-systems/ 
Tyler - https://www.darkreading.com/vulnerabilities-threats/linux-systems-are-becoming-bigger-targets

---

### Main Topic ### 
Should Schools Use FOSS?


---

### Apps of the Week ###

Matt - Betterbird 

Tyler - WeeChat
