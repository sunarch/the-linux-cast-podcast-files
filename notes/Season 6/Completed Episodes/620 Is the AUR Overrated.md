
# Season 6, Episode 20. Episode ID 84

### What have we been up to Linux related this Week? ###

Tyler - I think I can confidently say I will be using bspwm as my tiler from now on.

Matt – I started using the elecom huge again. It's actually good.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.omgubuntu.co.uk/2022/06/github-axes-atom-text-editor-recommends-vscode-instead 
Tyler - https://arstechnica.com/information-technology/2022/06/novel-techniques-in-never-before-seen-linux-backdoor-make-it-ultra-stealthy/

---

### Main Topic ### 
Is the AUR Overrated?


---

### Apps of the Week ###

Matt - Pokete https://github.com/lxgr-linux/pokete 

Tyler - MetaHuman Creator
