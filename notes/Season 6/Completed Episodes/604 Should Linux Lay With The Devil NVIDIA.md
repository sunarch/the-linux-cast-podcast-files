# Season 6, Episode 04. Episode ID 68

### What have we been up to Linux related this Week? ###

Tyler - I have hopped to ArcoLinux and am having a superb experience. 

Matt – I hopped to Manjaro and have been having an interesting time. 
---

**Contact Info**

Twitter: @thelinuxcast

Subscribe at http://thelinuxcast.org

Contact us email@thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Tyler - https://bit.ly/3wk9LNy on Odysee, https://bit.ly/3dbqbjX on YouTube

Discord https://discord.gg/89FRrrcGhC

Telegram https://t.me/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://9to5linux.com/12-year-old-policykit-local-privilege-escalation-flaw-now-patched-in-major-linux-distros 
Tyler - https://www.linuxtoday.com/developer/steam-deck-to-launch-officially-on-february-25th-2022/

---

### Main Topic ### 



---

### Apps of the Week ###

Matt - goyo 

Tyler - Atom
