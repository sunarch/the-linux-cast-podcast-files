# Season 6, Episode 18. Episode ID 82

### What have we been up to Linux related this Week?

Tyler - Been surprised by how much I like Debian, also I became a fish user O.o

Matt – Messing with Polybar, considering a change to a different distro.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?)

Matt - https://www.omgubuntu.co.uk/2022/05/ubuntu-22-10-makes-pipewire-default 
Tyler - https://9to5linux.com/almalinux-9-officially-released-based-on-red-hat-enterprise-linux-9

---

### Main Topic

Best Window Manager for Beginners

---

### Apps of the Week

Matt - Kingdoms And Castles on Steam & MA Profile Keycaps from KBDFans 

Tyler - Vampire Survivors
