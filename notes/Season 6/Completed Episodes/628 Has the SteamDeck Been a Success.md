
# Season 6, Episode 28. Episode ID 92

### What have we been up to Linux related this Week? ###

Tyler - Had a great birthday party last week, and am dog sitting this week and have been playing DayZ on the Steam Deck. It's fantastic, also tried Linux Mint on the Deck it was almost perfect.

Matt – Started using plex again, got a new microphone and audio interface, started to rework my standing desk set up a bit.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - [Linux 6.1 Will Make It A Bit Easier To Help Spot Faulty CPUs](https://www.phoronix.com/news/Linux-6.1-Seg-Fault-Report-CPU)
Tyler - [AMD Prepares More RDNA3 Code, New GPU Reset Mode For RX 6000 Series With Linux 6.1](https://www.phoronix.com/news/AMDGPU-Linux-6.1-Mode2-RDNA2)

---

### Main Topic ### 
Has the SteamDeck Been a Success?


---

### Apps of the Week ###

Matt - yt-dlp 

Tyler - Catppuccin
