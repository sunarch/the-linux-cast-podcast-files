
# Season 6, Episode 24. Episode ID 88

### What have we been up to Linux related this Week? ###

Tyler - Tried giving OpenBSD another a go. Didn't go well...

Matt – Still really loving Fedora, three weeks later. Learned some new polybar tricks for i3 this week. Also finishing up my kinoite review.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.omgubuntu.co.uk/2022/07/linux-mint-21-beta-released-this-is-whats-new 
Tyler - https://9to5linux.com/ventoy-multiboot-usb-creator-adds-support-for-fedora-coreos-more-than-940-isos

---

### Main Topic ### 
Best Desktop Environment


---

### Apps of the Week ###

Matt - Cool Retro Term 

Tyler - Steam Skins - Metro
