# Season 6, Episode 25. Episode ID 89

### What have we been up to Linux related this Week? ###

Tyler - Got A Stream Deck & Using Windows To Develop For It

Matt – Learning about Logs on Linux. Also installed Windows twice.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.omglinux.com/pine64-pinebuds-go-on-sale-october/ 
Tyler - https://www.gamingonlinux.com/2022/08/nara-facing-fire-inspired-by-hollow-knight-is-live-on-kickstarter-with-linux-support/

---

### Main Topic ### 
Arch vs Arch Based Distros - Which Should You Choose?


---

### Apps of the Week ###

Matt - Krusader 

Tyler - 
