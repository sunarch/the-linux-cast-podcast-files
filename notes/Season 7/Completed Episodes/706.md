# Season 7, Episode 06. Episode ID 106

---

What did we do in FOSS This Week?

---

## Matt News Links (2 Links)

- [Ubuntu Disallows Flavors From Using Flatpaks](https://www.omgubuntu.co.uk/2023/02/ubuntu-flavors-no-flatpak) 
- [Canonical Announce Availability of Real-Time Ubuntu](https://www.omgubuntu.co.uk/2023/02/real-time-ubuntu-kernel-general-availability)
## Tyler News Links (2 Links)

- [Linus Torvalds Reacts to Poorly-Executed Pull Requests for Linux 6.3](https://news.itsfoss.com/linus-torvalds-pull-request/)
- [Google Chrome's New Memory Saving Mode is Now Available for Linux](https://news.itsfoss.com/google-chrome-memory-linux/)

---

	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/contact/
	
	Steve - https://youtube.com/@XeroLinux
	
	Email - email@ thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---


## Josh News Links (2 Links)

 - [Linode brand is integrating Akami Connected Cloud](https://www.linode.com/blog/linode/a-bold-new-approach-to-the-cloud/)
 - [Linux 6.2 Released](https://9to5linux.com/linux-kernel-6-2-officially-released-this-is-whats-new)

## Steve News Links (2 Links)

- [KDE's Multi-Monitor Support Continues To Be Improved](https://www.phoronix.com/news/KDE-Better-Multi-Monitor-2023)
- [Linux 6.3 Bringing Proper Support For The 8BitDo Pro 2](https://www.phoronix.com/news/Linux-6.3-8BitDo-Pro-2-Wired)

---
## Apps of the Week

Matt - Upscaler 

Tyler - Webcord

Josh - [apt-mirror](https://apt-mirror.github.io/) --devs need help btw

Steve - [CryoUtilities 2.0](https://www.gamingonlinux.com/2023/02/cryoutilities-2-boosts-steam-deck-performance/)


