# Season 7, Episode 15. Episode ID 115
--

What did we do in FOSS This Week?

---

# Matt News Links

- [Microsoft is Courting Firefox](https://www.omgubuntu.co.uk/2023/05/microsoft-wants-firefox-to-ditch-google-switch-to-bing)
- [FX Technology tease their upgraded screen for Steam 
Deck](https://www.gamingonlinux.com/2023/05/fx-technology-tease-their-upgraded-screen-for-steam-deck/)

---
	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/stalker/
	
	Steve - https://youtube.com/@XeroLinux
	
	Email - email@thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---

# Steve News Links

- [ASUS Details ROG Ally Specs - $699 Pricetag](https://www.phoronix.com/news/ASUS-ROG-Ally)
- [KDE Plasma 6 Aiming For Wayland By Default](https://www.phoronix.com/news/Plasma-6-Defaults-Intent)

# Josh News Links

- [Weston 12 Features Multi GPU Support and Tearing Control](https://www.phoronix.com/news/Wayland-Weston-12.0)
- [Intel Dropping 32bit CPU support](https://www.phoronix.com/news/Intel-X86-S-64-bit-Only)

---
## Thingies of the Week 

Matt - [pywal](https://github.com/dylanaraps/pywal)

Steve - [AmazFit T-Rex 2 Watch](https://www.amazfit.com/products/amazfit-t-rex-2)

Josh - [sway](https://swaywm.org/) - *believe it or not*
