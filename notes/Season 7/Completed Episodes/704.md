# Season 7, Episode 03. Episode ID 103

---

What did we do in FOSS This Week?

---

## Matt News Links (2 Links)

- [XFCE 4.20 will bring Wayland Support](https://9to5linux.com/xfce-4-20-desktop-environment-will-finally-bring-wayland-support)
- [System76's Upcoming COSMIC Desktop is Gearing Up With Big Changes](https://news.itsfoss.com/system76-pop-os-cosmic-de-changes/)

---

	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/contact/
	
	Steve - https://youtube.com/@XeroLinux
	
	Contact Info at https://thelinuxcast.org/contact
	
---


## Josh News Links (2 Links)

 - [RedHat to host HackFest to implement HDR Support](https://fedoramagazine.org/announcing-the-display-hdr-hackfest/)
 - [KDE Plasma 6 Starts to Take Shape](https://pointieststick.com/2023/02/03/this-week-in-kde-plasma-6-starts-to-take-shape/)

## Steve News Links (2 Links)

- [Budgie 10.7 Desktop Environment is out](https://9to5linux.com/budgie-10-7-desktop-environment-adds-dual-gpu-support-new-power-dialog)
- [OBS Studio 29.0.1 Is Out to Fix Linux Crash on Wayland, X11 Capture Issue](https://9to5linux.com/obs-studio-29-0-1-is-out-to-fix-linux-crash-on-wayland-x11-capture-issue)

---
## Apps of the Week

Matt - Conky 

Tyler - 

Josh - [BTRFS Assistant](https://gitlab.com/btrfs-assistant/btrfs-assistant)

Steve - AirDroid (Nativefier)

