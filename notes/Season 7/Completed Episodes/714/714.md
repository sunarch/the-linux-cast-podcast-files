# Season 7, Episode 14. Episode ID 114
--

What did we do in FOSS This Week?

---

# Matt News Links

- [Debian's APT 2.7 Packaging Tool Begins Rolling Out "Snapshots" Support](https://www.phoronix.com/news/Debian-APT-2.7-Released)
- [Mozilla’s setting up shop on Mastodon and trying to reinvent content moderation](https://www.theverge.com/23710406/mozilla-social-mastodon-fediverse-moderation)

# Tyler News Links 
 
- [OURphone Is a Fully Open Source Smartphone Based on Linux](https://www.hackster.io/news/ourphone-is-a-fully-open-source-smartphone-based-on-linux-60aca685e87d)
- [RISC-V With Linux 6.4 Adds Hibernation / Suspend-To-Disk Support](https://www.phoronix.com/news/RISC-V-Hibernation-Support)


---
	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/stalker/
	
	Steve - https://youtube.com/@XeroLinux
	
	Email - email@thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---

# Steve News Links

-
-

# Josh News Links

- [Fedora KDE "Might" Drop X11 Entirely in Favor of Wayland](https://pagure.io/fedora-kde/SIG/issue/347)

- [Linux 6.4 makes pipes 10-24% faster](https://www.phoronix.com/news/Pipe-FMODE_NOWAIT-Linux-6.4)

---
## Thingies of the Week 

Matt - [Whisper](https://github.com/openai/whisper)

Steve - 

Tyler - MX Master 3

Josh - [PlasmaTube](https://flathub.org/apps/org.kde.plasmatube)
