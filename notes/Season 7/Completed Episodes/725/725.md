# Season 7, Episode 25. Episode ID 125
--

What did we do in FOSS This Week?

---

# Topic of the Week

Is Linux Really About Choice?



---
	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/stalker/
	
	Steve - https://fosstodon.org/@XeroLinux
	
	Email - email@thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---

# Fact of the Week: 

- By the year 2000 there were over 100 distros available to download. 

---

# Thingies of the Week

Matt - Cinny

Josh - [OhioLinuxFest 2023 September 8th/9th](https://olfconference.org/)

Tyler -

Steve - [Batocera](https://batocera.org)
